﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class ReporteFacturaEmpleado
    {
        //Reporte Factura
        private int codigo;
        private DateTime fecha;
        private float subtotal;
        private float iva;
        private float total;

        //Empleado
        private String nombre_empleado;
        private String apellido_empleado;

        public ReporteFacturaEmpleado()
        {
        }

        public ReporteFacturaEmpleado(int codigo, DateTime fecha, float subtotal, float iva, float total, string nombre_empleado, string apellido_empleado)
        {
            this.codigo = codigo;
            this.fecha = fecha;
            this.subtotal = subtotal;
            this.iva = iva;
            this.total = total;
            this.nombre_empleado = nombre_empleado;
            this.apellido_empleado = apellido_empleado;
        }

        public int Codigo { get => codigo; set => codigo = value; }
        public DateTime Fecha { get => fecha; set => fecha = value; }
        public float Subtotal { get => subtotal; set => subtotal = value; }
        public float Iva { get => iva; set => iva = value; }
        public float Total { get => total; set => total = value; }
        public string Nombre_empleado { get => nombre_empleado; set => nombre_empleado = value; }
        public string Apellido_empleado { get => apellido_empleado; set => apellido_empleado = value; }
    }
}
