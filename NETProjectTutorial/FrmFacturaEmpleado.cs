﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmFacturaEmpleado : Form
    {
        private DataSet dsSistema;
        private BindingSource bsProductoFacturaEmpleado;
        private DataTable tblFacturaEmpleados;
        private double subtotal;
        private double iva;
        private double total;
        private string cod_factura;


        public FrmFacturaEmpleado()
        {
            InitializeComponent();
            bsProductoFacturaEmpleado = new BindingSource();
        }

        public DataTable TblFacturaEmpleados { get => tblFacturaEmpleados; set => tblFacturaEmpleados = value; }
        public DataSet DsSistema { set => dsSistema = value; }

        private void BtnVer_Click(object sender, EventArgs e)
        {
            if (dsSistema.Tables["ReporteFacturaEmpleado"].Rows.Count == 0)
            {
                MessageBox.Show(this, "ERROR, No se puede generar la Factura, revise que hayan productos",
                    "Mensaje de error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataRow drFactura = dsSistema.Tables["Factura"].NewRow();
            drFactura["CodFactura"] = cod_factura;
            drFactura["Fecha"] = DateTime.Now;
            drFactura["Empleado"] = cmbEmpleados.SelectedValue;
            drFactura["SubTotal"] = subtotal;
            drFactura["Iva"] = iva;
            drFactura["Total"] = total;


            FrmReporteFacturaEmpleado frf = new FrmReporteFacturaEmpleado();
            frf.MdiParent = this.MdiParent;
            frf.DsSistema = dsSistema;
            frf.Show();

            Dispose();
        }

        private void CalcularTotalFactura()
        {
            subtotal = 0;
            foreach (DataRow dr in dsSistema.Tables["ProductoFactura"].Rows)
            {
                subtotal += Int32.Parse(dr["Cantidad"].ToString()) * Double.Parse(dr["Precio"].ToString());
            }

            iva = subtotal * 0.15;
            total = iva + subtotal;

            txtSubtotal.Text = subtotal.ToString();
            txtIva.Text = iva.ToString();
            txtTotal.Text = total.ToString();
        }

        private void FrmFacturaEmpleado_Load(object sender, EventArgs e)
        {
            cmbEmpleados.DataSource = dsSistema.Tables["Empleado"];
            cmbEmpleados.DisplayMember = "NA";
            cmbEmpleados.ValueMember = "Id";

            Fecha.Text = DateTime.Now.ToString();
        }
    }
}
