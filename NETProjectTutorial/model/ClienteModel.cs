﻿using NETProjectTutorial.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class ClienteModel
    {
        private static List<Cliente> ListCliente = new List<Cliente>();

        public static List<Cliente> GetListCliente()
        {
            return ListCliente;
        }


        public static void Populate()
        {
            ListCliente = JsonConvert.DeserializeObject<List<Cliente>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.DataClients));
        }
    }
}

