﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmReporteFacturaEmpleado : Form
    {
        private DataSet dsSistema;
        private DataRow dtSistema;


        public FrmReporteFacturaEmpleado()
        {
            InitializeComponent();
        }

        public DataSet DsSistema { get => dsSistema; set => dsSistema = value; }
        public DataRow DtSistema {set => dtSistema = value; }

        private void FrmReporteFacturaEmpleado_Load(object sender, EventArgs e)
        {
            dsSistema.Tables["ReporteFacturaEmpleado"].Rows.Clear();

            DataTable dtFactura = dsSistema.Tables["Factura"];
            int countFactura = dtFactura.Rows.Count;

            DataRow drFactura = dtFactura.Rows[countFactura - 1];
            DataRow drEmpleado = dsSistema.Tables["Empleado"].Rows.Find(drFactura["Empleado"]);

            DataTable dtDetalleFactura = dsSistema.Tables["DetalleFactura"];

            DataRow[] drDetallesFacturas = dtDetalleFactura.Select(String.Format("Factura = {0}", drFactura["Id"]));

            foreach (DataRow dr in drDetallesFacturas)
            {
                DataRow drReporteFactura = dsSistema.Tables["ReporteFactura"].NewRow();
                DataRow drProducto = dsSistema.Tables["Producto"].Rows.Find(dr["Producto"]);
                drReporteFactura["Cod_Factura"] = drFactura["CodFactura"];
                drReporteFactura["Fecha"] = drFactura["Fecha"];
                drReporteFactura["Subtotal"] = drFactura["Subtotal"];
                drReporteFactura["IVA"] = drFactura["Iva"];
                drReporteFactura["Total"] = drFactura["Total"];
                drReporteFactura["Nombre_Empleado"] = drEmpleado["Nombres"];
                drReporteFactura["Apellido_Empleado"] = drEmpleado["Apellidos"];

                dsSistema.Tables["ReporteFacturaEmpleado"].Rows.Add(drReporteFactura);
            }

            this.reportViewer1.LocalReport.ReportEmbeddedResource = "NETProjectTutorial.ReporteFacturaEmpleado.rdlc";
            ReportDataSource rds1 = new ReportDataSource("dsReporteFacturaEmpleado", dsSistema.Tables["ReporteFacturaEmpleado"]);
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(rds1);


            this.reportViewer1.RefreshReport();
        }
    }
}
